import javax.swing.*;
import java.awt.*;
import java.awt.event.ItemListener;
import java.io.File;

/**
 * Created by Bartosz Szkolnik on 12.03.2017.
 */
public class MainWindow {
    private final int frameHeight = 350;
    private final int frameWidth = 300;
    private JFrame mainFrame;
    private CardLayout cardLayout;
    private JPanel cards;
    private JComboBox cb;


    MainWindow() {
        this.mainFrame = new JFrame("Main window");
    }

    public void init() {
        configureFrame();
        createFolderIfNecessary();
        configureLayout();
    }

    public void showFrame() {
        this.mainFrame.setVisible(true);
    }

    private void createFolderIfNecessary() {
        String OS = System.getProperty("os.name").toLowerCase();
        File text_files;

        if (OS.equals("windows 10")) {
            try {
                text_files = new File(".\\text-files");
                if(!text_files.exists()) {
                    text_files.mkdir();
                }
            } catch(SecurityException e) {
                JOptionPane.showMessageDialog(mainFrame,
                        "Couldn't create folder for user data.");
            }

        }
    }

    public void closeFrame() {
        this.mainFrame.setVisible(false);
        this.mainFrame.dispose();
    }

    private void configureFrame() {
        mainFrame.setDefaultCloseOperation(mainFrame.EXIT_ON_CLOSE);
        mainFrame.setSize(frameWidth, frameHeight);
        mainFrame.setLocationRelativeTo(null);
    }

    private void configureLayout() {
        Container contentPane = mainFrame.getContentPane();

        LogInWindow logInWindow = new LogInWindow(this);
        logInWindow.init();

        RegisterWindow registerWindow = new RegisterWindow(this);
        registerWindow.init();

        cardLayout = new CardLayout();
        cards = new JPanel(cardLayout);
        cards.add(logInWindow.returnPanel(), "Log In");
        cards.add(registerWindow.returnPanel(), "Registry");

        JPanel comboBoxPane = new JPanel();

        String[] comboBoxItems = {"Log In", "Registry"};
        cb = new JComboBox(comboBoxItems);
        cb.setEditable(false);

        ItemListener itemListener = (e) -> cardLayout.show(cards, e.getItem().toString());
        cb.addItemListener(itemListener);

        comboBoxPane.add(cb);

        contentPane.add(comboBoxPane, BorderLayout.PAGE_START);
        contentPane.add(cards, BorderLayout.CENTER);

        cardLayout.show(cards, "Log In");
    }

    public void setCardToLoggingIn() {
        cardLayout.show(cards, "Log in");
        cb.setSelectedIndex(0);
    }
}