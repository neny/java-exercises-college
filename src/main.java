/**
 * Created by Bartosz Szkolnik on 12.03.2017.
 */
public class main {
    public static void main (String[] args) {
        MainWindow mainWindow = new MainWindow();
        mainWindow.init();
        mainWindow.showFrame();
    }
}
