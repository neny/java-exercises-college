import javax.swing.*;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.regex.Pattern;

/**
 * Created by Bartosz Szkolnik on 26.03.2017.
 */
public class RegisterLogic {
    private JPanel registerPanel;

    public RegisterLogic(JPanel registerPanel) {
        this.registerPanel = registerPanel;
    }

    public boolean checkText(String text) {
        boolean test = Pattern.matches("[a-zA-Z]+",text);
        return test;
    }

    public boolean checkEmail(String text) {
        //Wyrażenie regularne znalezione na stronie http://emailregex.com/ , które to sprawdza czy podany adres e-mail
        // jest poprawny.
        final String EmailValidationPattern = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+" +
                ")*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\" +
                "x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?" +
                "|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|" +
                "[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\" +
                "x0b\\x0c\\x0e-\\x7f])+)\\])";

        boolean test = Pattern.matches(EmailValidationPattern,text);
        return test;
    }

    public boolean checkAge(String age) {
        byte temp = 0;
        if (age.isEmpty()) {
            return false;
        }
        try {
            temp = Byte.parseByte(age);
            return true;
        } catch (NumberFormatException ez) {
            return false;
        }
    }

    public void writeDataToFile(String name, String surname, String email, byte age, String password, String filePath, File file) {
        if (file.canWrite()) {
            try {
                FileWriter writer = new FileWriter(filePath);
                writer.write(password + ";");
                writer.write(name + ";");
                writer.write(surname + ";");
                writer.write(email + ";");
                writer.write(age + ";");
                writer.close();
            } catch (IOException e) {
                JOptionPane.showMessageDialog(registerPanel,
                        "Some error with saving data appeared. Please try again.",
                        "Saving error",
                        JOptionPane.ERROR_MESSAGE);
            }
        } else {
            JOptionPane.showMessageDialog(registerPanel,
                    "Some error with access to file appeared. Please try again.",
                    "File access error",
                    JOptionPane.ERROR_MESSAGE);
        }
    }
}
