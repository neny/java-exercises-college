import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;

import java.awt.Container;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.io.File;
import java.io.IOException;


/**
 * Created by Bartosz Szkolnik on 13.03.2017.
 */
public class RegisterWindow {
    private JPanel registerPanel;
    private SpringLayout layout;
    private MainWindow mainWindow;
    private JTextField nameField;
    private JTextField surnameField;
    private JTextField emailField;
    private JTextField ageField;
    private JTextField loginField;
    private JPasswordField passwordField;
    private JLabel statusLabel;
    private boolean registered = false;
    private String filePath;
    private RegisterLogic registerLogic;

    public RegisterWindow(MainWindow mainWindow) {
        registerPanel = new JPanel();
        this.mainWindow = mainWindow;
        this.registerLogic = new RegisterLogic(registerPanel);
    }

    private void configureLayout() {
        this.layout = new SpringLayout();
        registerPanel.setLayout(layout);
    }

    public void init() {
        configureLayout();
        addAcceptButton();
        addCloseButton();
        addLabelsAndTextFields();
        addStatusLabel();
    }

    public JPanel returnPanel () {
        return this.registerPanel;
    }

    private void addAcceptButton() {
        JButton acceptButton = new JButton("Accept");
        registerPanel.add(acceptButton);
        acceptButton.addActionListener((e) -> acceptButtonClicked());
        layout.putConstraint(SpringLayout.HORIZONTAL_CENTER, acceptButton, -50, SpringLayout.HORIZONTAL_CENTER, registerPanel);
        layout.putConstraint(SpringLayout.NORTH, acceptButton, 230, SpringLayout.NORTH, registerPanel);
    }

    private void acceptButtonClicked() {
        String name = nameField.getText();
        String surname = surnameField.getText();
        String email = emailField.getText();
        String age = ageField.getText();
        String login = loginField.getText();
        char[] t = passwordField.getPassword();
        String password = new String(t);

        if (!(name.isEmpty()) && registerLogic.checkText(name) &&
                !(surname.isEmpty()) && registerLogic.checkText(surname) &&
                !(email.isEmpty()) && registerLogic.checkEmail(email) &&
                !(age.isEmpty()) &&  registerLogic.checkAge(age) &&
                !(login.isEmpty()) && !(password.isEmpty())) {

            setData(name, surname, email, Byte.parseByte(age), login, password);

                if (registered) {
                    this.mainWindow.setCardToLoggingIn();
                    nameField.setText("");
                    surnameField.setText("");
                    emailField.setText("");
                    ageField.setText("");
                    loginField.setText("");
                    passwordField.setText("");
                    statusLabel.setText("");
                }
        } else {
            statusLabel.setText("Something you passed is not valid");
        }
    }

    private void addCloseButton() {
        JButton closeButton= new JButton("Close");
        registerPanel.add(closeButton);
        closeButton.addActionListener((e) -> mainWindow.closeFrame());
        layout.putConstraint(SpringLayout.HORIZONTAL_CENTER, closeButton, 50, SpringLayout.HORIZONTAL_CENTER, registerPanel);
        layout.putConstraint(SpringLayout.NORTH, closeButton, 230, SpringLayout.NORTH, registerPanel);
    }

    private void addLabelsAndTextFields() {
        JPanel addressPanel = new JPanel();
        Border border = addressPanel.getBorder();
        Border margin = new EmptyBorder(10, 10, 10, 10);
        addressPanel.setBorder(new CompoundBorder(border, margin));

        GridBagLayout panelGridBagLayout = new GridBagLayout();
        panelGridBagLayout.columnWidths = new int[] { 86, 86, 0 };
        panelGridBagLayout.rowHeights = new int[] { 20, 20, 20, 20, 20, 0 };
        panelGridBagLayout.columnWeights = new double[] { 0.0, 1.0, Double.MIN_VALUE };
        panelGridBagLayout.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
        addressPanel.setLayout(panelGridBagLayout);

        addNameLabelAndTextField("Name: ", 0, addressPanel);
        addSurnameLabelAndTextField("Surname: ", 1, addressPanel);
        addEmailLabelAndTextField("Email: ", 2, addressPanel);
        addAgeLabelAndTextField("Age: ", 3, addressPanel);
        addLoginLabelAndTextField("Login: ", 4, addressPanel);
        addLabelAndPasswordField(5, addressPanel);

        registerPanel.add(addressPanel);
    }

    private void addLabel(String labelText, int yPos, Container addressPanel) {
        JLabel faxLabel = new JLabel(labelText);
        faxLabel.setHorizontalAlignment(SwingConstants.RIGHT);
        GridBagConstraints gridBagConstraintForLabel = new GridBagConstraints();
        gridBagConstraintForLabel.fill = GridBagConstraints.BOTH;
        gridBagConstraintForLabel.insets = new Insets(0, 0, 5, 0);
        gridBagConstraintForLabel.gridx = 0;
        gridBagConstraintForLabel.gridy = yPos;

        addressPanel.add(faxLabel, gridBagConstraintForLabel);
    }

    private void configureTextField(int yPos, Container addressPanel, JTextField field) {
        GridBagConstraints gridBagConstraintForTextField = new GridBagConstraints();
        gridBagConstraintForTextField.fill = GridBagConstraints.BOTH;
        gridBagConstraintForTextField.insets = new Insets(0, 0, 5, 0);
        gridBagConstraintForTextField.gridx = 1;
        gridBagConstraintForTextField.gridy = yPos;
        addressPanel.add(field, gridBagConstraintForTextField);
    }

    private void addNameLabelAndTextField(String labelText, int yPos, Container addressPanel) {
        addLabel(labelText, yPos, addressPanel);
        nameField = new JTextField("");
        configureTextField(yPos, addressPanel, nameField);
        nameField.setColumns(10);
    }

    private void addSurnameLabelAndTextField(String labelText, int yPos, Container addressPanel) {
        addLabel(labelText, yPos, addressPanel);
        surnameField = new JTextField("");
        configureTextField(yPos, addressPanel, surnameField);
        surnameField.setColumns(10);
    }

    private void addEmailLabelAndTextField(String labelText, int yPos, Container addressPanel) {
        addLabel(labelText, yPos, addressPanel);
        emailField = new JTextField("");
        configureTextField(yPos, addressPanel, emailField);
        emailField.setColumns(10);
    }

    private void addAgeLabelAndTextField(String labelText, int yPos, Container addressPanel) {
        addLabel(labelText, yPos, addressPanel);
        ageField = new JTextField("");
        configureTextField(yPos, addressPanel, ageField);
        ageField.setColumns(10);
    }

    private void addLoginLabelAndTextField(String labelText, int yPos, Container addressPanel) {
        addLabel(labelText, yPos, addressPanel);
        loginField = new JTextField("");
        configureTextField(yPos, addressPanel, loginField);
        loginField.setColumns(10);
    }

    private void addLabelAndPasswordField(int yPos, Container addressPanel) {
        addLabel("Password: ", yPos, addressPanel);
        passwordField = new JPasswordField("");
        configureTextField(yPos, addressPanel, passwordField);
        passwordField.setColumns(10);
    }

    private void addStatusLabel () {
        statusLabel = new JLabel("");
        registerPanel.add(statusLabel);
        layout.putConstraint(SpringLayout.HORIZONTAL_CENTER, statusLabel, 0, SpringLayout.HORIZONTAL_CENTER, registerPanel);
        layout.putConstraint(SpringLayout.NORTH, statusLabel, 200, SpringLayout.NORTH, registerPanel);
    }

    private void setFilePath(String login) {
        String OS = System.getProperty("os.name").toLowerCase();
        File directory = new File(".\\text-files");

        if (directory.exists() && OS.equals("windows 10")) {
            filePath = ".\\text-files\\" + login + ".txt";
        } else if (OS.equals("linux")) {
            filePath = login + ".txt";
        } else { //unknown system
            filePath = login + ".txt";
        }
    }

    public void setData(String name, String surname, String email, byte age, String login, String password) {
        setFilePath(login);

        File file = new File(filePath);

        if (!file.exists()) {
            try {
                file.createNewFile();
                registered = true;
                registerLogic.writeDataToFile(name, surname, email, age, password, filePath, file);
            } catch (IOException e) {
                JOptionPane.showMessageDialog(registerPanel,
                        "Some error with saving data appeared. Please try again.",
                        "Saving error",
                        JOptionPane.ERROR_MESSAGE);
            }
        } else {
            statusLabel.setText("That user already exists.");
        }
    }
}