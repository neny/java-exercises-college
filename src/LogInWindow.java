import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;

import java.awt.*;
import java.io.*;

/**
 * Created by Bartosz Szkolnik on 13.03.2017.
 */
public class LogInWindow {
    private JPanel logInPanel;
    private MainWindow mainWindow;
    private SpringLayout layout;
    private JTextField loginField;
    private JPasswordField passwordField;
    private JLabel statusLabel;
    private String name;
    private String surname;
    private String email;
    private byte age;
    private String password;
    private boolean loggedIn = false;
    private String filePath;

    public LogInWindow(MainWindow mainWindow) {
        logInPanel = new JPanel();
        this.mainWindow = mainWindow;
    }

    private void configureLayout() {
        this.layout = new SpringLayout();
        logInPanel.setLayout(layout);
    }

    public void init() {
        configureLayout();
        paintWhenNotLoggedIn();
    }

    public JPanel returnPanel() {
        return this.logInPanel;
    }

    private void paintWhenNotLoggedIn() {
        addLogInButton();
        addCloseButton();
        addLabelsAndTextFields();
        addStatusLabel();
    }

    private void paintWhenLoggedIn() {
        addLoggedInLabels();
        addLogOutButton();
        addCloseButton();
    }

    private void addLoggedInLabels() {
        addFirstLoggedInLabel();
        addSecondLoggedInLabel();
        addThirdLoggedInLabel();
    }

    private void addFirstLoggedInLabel() {
        JLabel label = new JLabel("Your name is " + this.name + " " + this.surname + "." );
        layout.putConstraint(SpringLayout.HORIZONTAL_CENTER, label, 0, SpringLayout.HORIZONTAL_CENTER, logInPanel);
        layout.putConstraint(SpringLayout.NORTH, label, 30, SpringLayout.NORTH, logInPanel);
        logInPanel.add(label);
    }

    private void addSecondLoggedInLabel() {
        JLabel label = new JLabel("You are " + this.age + " years old.");
        layout.putConstraint(SpringLayout.HORIZONTAL_CENTER, label, 0, SpringLayout.HORIZONTAL_CENTER, logInPanel);
        layout.putConstraint(SpringLayout.NORTH, label, 60, SpringLayout.NORTH, logInPanel);
        logInPanel.add(label);
    }

    private void addThirdLoggedInLabel() {
        JLabel label = new JLabel("Your email is " + this.email + " .");
        layout.putConstraint(SpringLayout.HORIZONTAL_CENTER, label, 0, SpringLayout.HORIZONTAL_CENTER, logInPanel);
        layout.putConstraint(SpringLayout.NORTH, label, 90, SpringLayout.NORTH, logInPanel);
        logInPanel.add(label);
    }

    private void addLogInButton() {
        JButton acceptButton = new JButton("Log in");
        logInPanel.add(acceptButton);
        acceptButton.addActionListener((e) -> logInButtonClicked());
        layout.putConstraint(SpringLayout.HORIZONTAL_CENTER, acceptButton, -50, SpringLayout.HORIZONTAL_CENTER, logInPanel);
        layout.putConstraint(SpringLayout.NORTH, acceptButton, 230, SpringLayout.NORTH, logInPanel);
    }

    private void logInButtonClicked() {
        String login = loginField.getText();
        char[] t = passwordField.getPassword();
        String password = new String(t);

        if (checkData(login, password) && !this.loggedIn) {
            logInPanel.removeAll();
            logInPanel.revalidate();
            logInPanel.repaint();
//            logInPanel.repaint();

            this.loggedIn = true;

            paintWhenLoggedIn();
        } else {
            statusLabel.setText("Login or password wrong");
        }
    }

    private void addLogOutButton() {
        JButton logOutButton = new JButton("Log out");
        logInPanel.add(logOutButton);
        logOutButton.addActionListener((e) -> logOutButtonClicked());
        layout.putConstraint(SpringLayout.HORIZONTAL_CENTER, logOutButton, -50, SpringLayout.HORIZONTAL_CENTER, logInPanel);
        layout.putConstraint(SpringLayout.NORTH, logOutButton, 230, SpringLayout.NORTH, logInPanel);
    }

    private void logOutButtonClicked() {
        this.loggedIn = false;
        logInPanel.removeAll();
        logInPanel.revalidate();
        logInPanel.repaint();

        paintWhenNotLoggedIn();
    }

    private void addCloseButton() {
        JButton closeButton= new JButton("Close");
        logInPanel.add(closeButton);
        closeButton.addActionListener((e) -> this.mainWindow.closeFrame());
        layout.putConstraint(SpringLayout.HORIZONTAL_CENTER, closeButton, 50, SpringLayout.HORIZONTAL_CENTER, logInPanel);
        layout.putConstraint(SpringLayout.NORTH, closeButton, 230, SpringLayout.NORTH, logInPanel);
    }

    private void addLabelsAndTextFields() {
        JPanel addressPanel = new JPanel();
        Border border = addressPanel.getBorder();
        Border margin = new EmptyBorder(10, 10, 10, 10);
        addressPanel.setBorder(new CompoundBorder(border, margin));

        GridBagLayout panelGridBagLayout = new GridBagLayout();
        panelGridBagLayout.columnWidths = new int[] { 86, 86, 0 };
        panelGridBagLayout.rowHeights = new int[] { 20, 20, 20, 20, 20, 0 };
        panelGridBagLayout.columnWeights = new double[] { 0.0, 1.0, Double.MIN_VALUE };
        panelGridBagLayout.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
        addressPanel.setLayout(panelGridBagLayout);

        addLoginLabelAndTextField("Login: ", 0, addressPanel);
        addLabelAndPasswordField(1, addressPanel);


        logInPanel.add(addressPanel);
    }

    private void addLabel(String labelText, int yPos, Container addressPanel) {
        JLabel faxLabel = new JLabel(labelText);
        faxLabel.setHorizontalAlignment(SwingConstants.RIGHT);
        GridBagConstraints gridBagConstraintForLabel = new GridBagConstraints();
        gridBagConstraintForLabel.fill = GridBagConstraints.BOTH;
        gridBagConstraintForLabel.insets = new Insets(0, 0, 5, 5);
        gridBagConstraintForLabel.gridx = 0;
        gridBagConstraintForLabel.gridy = yPos;

        addressPanel.add(faxLabel, gridBagConstraintForLabel);
    }

    private void configureTextField(int yPos, Container addressPanel, JTextField field) {
        GridBagConstraints gridBagConstraintForTextField = new GridBagConstraints();
        gridBagConstraintForTextField.fill = GridBagConstraints.BOTH;
        gridBagConstraintForTextField.insets = new Insets(0, 0, 5, 0);
        gridBagConstraintForTextField.gridx = 1;
        gridBagConstraintForTextField.gridy = yPos;
        addressPanel.add(field, gridBagConstraintForTextField);
    }

    private void addLoginLabelAndTextField(String labelText, int yPos, Container addressPanel) {
        addLabel(labelText, yPos, addressPanel);
        loginField = new JTextField();
        configureTextField(yPos, addressPanel, loginField);
        loginField.setColumns(10);
    }

    private void addLabelAndPasswordField(int yPos, Container addressPanel) {
        addLabel("Password: ", yPos, addressPanel);
        passwordField = new JPasswordField();
        configureTextField(yPos, addressPanel, passwordField);
        passwordField.setColumns(10);
    }

    private void addStatusLabel () {
        statusLabel = new JLabel("");
        logInPanel.add(statusLabel);
        layout.putConstraint(SpringLayout.HORIZONTAL_CENTER, statusLabel, 0, SpringLayout.HORIZONTAL_CENTER, logInPanel);
        layout.putConstraint(SpringLayout.NORTH, statusLabel, 200, SpringLayout.NORTH, logInPanel);
    }

    private void setFilePath(String login) {
        String OS = System.getProperty("os.name").toLowerCase();
        File directory = new File(".\\text-files");

        if (directory.exists() && OS.equals("windows 10")) {
            filePath = ".\\text-files\\" + login + ".txt";
        } else if (OS.equals("linux")) {
            filePath = login + ".txt";
        } else { //unknown system
            filePath = login + ".txt";
        }
    }

    private boolean checkData (String login, String password) {
        setFilePath(login);
        File file = new File(filePath);

        if (!file.exists()) {
            statusLabel.setText("That user does not exist");
            return false;
        } else {
            getDataFromFile(filePath);
             if (password.equals(this.password)) {
                 System.out.println("test");
                 return true;
             } else {
                 return false;
             }
        }
    }

    public void getDataFromFile(String filePath) {
        FileReader reader;
        BufferedReader bufferedReader;
        try {
            reader = new FileReader(filePath);
            bufferedReader = new BufferedReader(reader);

            String line = bufferedReader.readLine();
            String[] words = line.split(";");

            bufferedReader.close();
            reader.close();

            this.password = words[0];
            this.name = words[1];
            this.surname = words[2];
            this.email = words[3];
            this.age = Byte.parseByte(words[4]);
        }
        catch (FileNotFoundException e) {
            JOptionPane.showMessageDialog(logInPanel,
                    "File could not be found. Please try again.",
                    "Saving error",
                    JOptionPane.ERROR_MESSAGE);
        } catch (IOException e) {
            JOptionPane.showMessageDialog(logInPanel,
                    "Some error with gathering data appeared. Please try again.",
                    "Getting data error",
                    JOptionPane.ERROR_MESSAGE);
        }
    }
}
